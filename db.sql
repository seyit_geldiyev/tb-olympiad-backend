DROP TABLE IF EXISTS tbl_sport_complex, tbl_sport_complex_types;

CREATE TABLE tbl_sport_complex (
    id SERIAL PRIMARY KEY,
    name_ru VARCHAR(250) NOT NULL,
    name_en VARCHAR(250),
    short_description_ru TEXT,
    short_description_en TEXT,
    description_ru TEXT,
    description_en TEXT,
    type_of_sport_complex VARCHAR(100),
    type_of_sports TEXT,
    address_ru TEXT,
    address_en TEXT,
    competition VARCHAR(50),
    yandex_coord_x FLOAT NOT NULL,
    yandex_coord_y FLOAT NOT NULL,
    yandex_coord_center_x FLOAT NOT NULL,
    yandex_coord_center_y FLOAT NOT NULL,
    yandex_zoom INTEGER NOT NULL,
    work_time_mn_fr VARCHAR(100),
    work_time_st VARCHAR(100),
    work_time_sn VARCHAR(100),
    email VARCHAR(100),
    contact_phone VARCHAR(100),
    website_url VARCHAR(100),
    significance VARCHAR(100),
    active BOOLEAN,
    created_ts timestamp DEFAULT CURRENT_TIMESTAMP
)

CREATE TABLE tbl_sport_complex_types (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    count INTEGER
)