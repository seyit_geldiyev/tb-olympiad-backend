import pg from "@/config/db";
import { ApiSportComplexListQuery } from "@/types/request/SportComplex";
import type {
  ApiSportComplex,
  ApiSportComplexType,
} from "@/types/response/SportComplex";

export class SportComplexDAO {
  getList = (
    query: ApiSportComplexListQuery,
  ): Promise<ApiSportComplex[] | any> => {
    const LIMIT = query.limit || 9;
    const offset = query.offset || 0;
    const sportComplexType = query.sport_complex_type || "";
    return Promise.all([
      pg.query(
        "SELECT id, name_ru, name_en, short_description_ru, short_description_en, active FROM tbl_sport_complex WHERE type_of_sport_complex LIKE $3 LIMIT $1 OFFSET $2",
        [LIMIT, offset, "%" + sportComplexType + "%"],
      ),
      pg.query("SELECT COUNT(*) from tbl_sport_complex"),
    ]);
  };

  get = (id?: number): Promise<ApiSportComplex | any> => {
    return Promise.resolve(
      pg.query("SELECT * FROM tbl_sport_complex WHERE id = $1", [id]),
    );
  };

  getTypeList = (): Promise<ApiSportComplexType[] | any> => {
    return Promise.resolve(pg.query("SELECT * FROM tbl_sport_complex_types"));
  };
}
