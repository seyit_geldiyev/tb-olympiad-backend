import { Router } from "express";
import { HomeController, appKeyValidator } from "@/components/home";
import { sanitizer } from "@/helpers";
import { SportComplexController } from "@/components/sport-complex";

const router = Router();

router.get("/", sanitizer(appKeyValidator), HomeController.getAppInfo);
router.get("/sport-complex/list", SportComplexController.getSportComplexList);
router.get(
  "/sport-complex/type/list",
  SportComplexController.getSportComplexTypeList,
);
router.get("/sport-complex/get", SportComplexController.getSportComplex);

export default router;
