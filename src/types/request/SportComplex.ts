export type ApiSportComplexListQuery = {
  limit?: number;
  offset?: number;
  sport_complex_type?: string;
};
