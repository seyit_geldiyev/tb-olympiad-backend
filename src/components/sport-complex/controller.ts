import { OK } from "http-status/lib";
import { SportComplexServices } from "./services";
import { apiResponse } from "@/helpers/apiResponse";
import { ApiSportComplexListQuery } from "@/types/request/SportComplex";

export class SportComplexController {
  static getSportComplexList = async (req: Req, res: Res, next: NextFn) => {
    try {
      const query = req.query as ApiSportComplexListQuery;
      const sportComplexServices = new SportComplexServices();
      const result = await sportComplexServices.getSportComplexList(query);

      res.status(OK).json(apiResponse(result));
    } catch (error) {
      next(error);
    }
  };

  static getSportComplex = async (req: Req, res: Res, next: NextFn) => {
    try {
      const id = Number(req.query.id);
      const sportComplexServices = new SportComplexServices();
      const result = await sportComplexServices.getSportComplex(id);

      res.status(OK).json(apiResponse(result));
    } catch (error) {
      next(error);
    }
  };

  static getSportComplexTypeList = async (req: Req, res: Res, next: NextFn) => {
    try {
      const {} = req.query;
      const sportComplexServices = new SportComplexServices();
      const result = await sportComplexServices.getSportComplexTypeList();

      res.status(OK).json(apiResponse(result));
    } catch (error) {
      next(error);
    }
  };
}
