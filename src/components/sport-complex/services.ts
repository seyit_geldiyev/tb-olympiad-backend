import { SportComplexDAO } from "@/db/sport-complex";
import { ApiSportComplexListQuery } from "@/types/request/SportComplex";
import type {
  ApiSportComplex,
  ApiSportComplexList,
  ApiSportComplexType,
} from "@/types/response/SportComplex";

export class SportComplexServices {
  sportComplexDAO!: SportComplexDAO;
  constructor() {
    this.sportComplexDAO = new SportComplexDAO();
  }

  getSportComplexList = async (
    query: ApiSportComplexListQuery,
  ): Promise<ApiSportComplexList> => {
    const result = await this.sportComplexDAO.getList(query);

    return {
      total_count: result[1].rows[0].count,
      list: result[0].rows,
    };
  };

  getSportComplex = async (id?: number): Promise<ApiSportComplex> => {
    const result = await this.sportComplexDAO.get(id);

    return result.rows[0];
  };

  getSportComplexTypeList = async (): Promise<ApiSportComplexType[]> => {
    const result = await this.sportComplexDAO.getTypeList();

    return result.rows;
  };
}
